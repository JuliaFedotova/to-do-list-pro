import fs from 'fs';
import {getFileContent} from "./utils/getFileContent.js";
import {Note} from "./interfaces/note.interface";

const totoService = {
    addNote(title: string) {
        const notesList = getFileContent<Note[]>('./data/notes.json');
        const notesAmount = notesList.length;
        const id = notesAmount > 0 ? notesList[notesAmount - 1].id + 1 : 1;
        const deleted_at = '';
        const NoteStatus = '';
        const newObject = {
            title,
            id,
            deleted_at,
            NoteStatus,
        }

        notesList.push(newObject);

        fs.writeFileSync('./data/notes.json', JSON.stringify(notesList));

        return newObject;
    },

    restoreNote(notesId: number) {
        const notesList = getFileContent<Note[]>('./data/notes.json');;
        const index = notesList.findIndex(note => note.id == notesId);

        if (index >= 0) {
            notesList[index].deleted_at = '';

            fs.writeFileSync('./data/notes.json', JSON.stringify(notesList));

            return notesList.filter(note => note.deleted_at !== '');

        } else {
            throw new Error('Note not found')
        }
    },

    uncheckNote(notesId: number) {
        const notesList = getFileContent<Note[]>('./data/notes.json');;
        const index = notesList.findIndex(note => note.id == notesId);

        if (index >= 0) {
            notesList[index].NoteStatus = '';

            fs.writeFileSync('./data/notes.json', JSON.stringify(notesList));

            return notesList.filter(note => note.NoteStatus !== '');

        } else {
            throw new Error('Note not found')
        }
    },

    checkNote(notesId: number) {
        const notesList = getFileContent<Note[]>('./data/notes.json');;
        const index = notesList.findIndex(note => note.id == notesId);

        if (index >= 0) {
            notesList[index].NoteStatus = 'checked';

            fs.writeFileSync('./data/notes.json', JSON.stringify(notesList));

            return notesList.filter(note => note.NoteStatus === '' && note.deleted_at === '');
        } else {
            throw new Error('Note not found')
        }
    },

    deleteNote(notesId: number) {
        const notesList = getFileContent<Note[]>('./data/notes.json');;
        const index = notesList.findIndex(note => note.id == notesId);


        if (index >= 0) {
            notesList[index].deleted_at = new Date().toString();

            fs.writeFileSync('./data/notes.json', JSON.stringify(notesList));

            return notesList.filter(note => note.deleted_at === '' && note.NoteStatus === '');

        } else {
            throw new Error('Note not found');
        }
    },

    getNote() {
        const notesList = getFileContent<Note[]>('./data/notes.json');;
        return notesList.filter(note => note.deleted_at === '' && note.NoteStatus === '');
    },

    getDeletedNotes() {
        const notesList = getFileContent<Note[]>('./data/notes.json');;
        return notesList.filter(note => note.deleted_at !== '');

    },

    getCheckedNotes() {
        const notesList = getFileContent<Note[]>('./data/notes.json');;
        return notesList.filter(note => note.NoteStatus !== '');
    },

    updateNote(notesId: number, title: string) {
        const notesList = getFileContent<Note[]>('./data/notes.json');;
        const index = notesList.findIndex(note => note.id == notesId);

        if (index >= 0) {
            notesList[index] = {
                ...notesList[index],
                title,
            }

            fs.writeFileSync('./data/notes.json', JSON.stringify(notesList));
            return notesList[index];

        } else {
            throw new Error('Note not found');
        }
    }
}

export default totoService;