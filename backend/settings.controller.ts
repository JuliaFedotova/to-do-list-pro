import { Router } from 'express';
import settingsService from './settings.service.js';
// @ts-ignore
import multipart from 'connect-multiparty';
import {getErrorMessage} from "./utils/getErrorMessage.js";

const router = Router();
const multipartMiddleware = multipart();

router.post('/settings', multipartMiddleware, (req, res) => {
    try {
        res.send(settingsService.addImage(req.body.backgroundPath));
    } catch (e) {
        res.status(500);
        res.send(getErrorMessage(e));
    }
})

router.get('/settings', (req, res) => {
    try {
        res.send(settingsService.getSettings());
    } catch (e) {
        res.status(500);
        res.send(getErrorMessage(e));
    }
})

export default router;