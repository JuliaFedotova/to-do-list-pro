import fs from "fs";

export function getFileContent<T>(filePath: string): T {
    const imagesListString = fs.readFileSync(filePath, "utf8");
    return JSON.parse(imagesListString);
}