import {Settings} from "./interfaces/settings.interface";
import fs from "fs";
import {getFileContent} from "./utils/getFileContent.js";

const settingsService = {

    addImage(backgroundPath: string) {
        const settings = getFileContent<Settings>('./data/settings.json');
        const updatedSettings = {
            ...settings,
            backgroundPath
        }

        fs.writeFileSync('./data/settings.json', JSON.stringify(updatedSettings));

        return updatedSettings;
    },

    getSettings() {
         return getFileContent<Settings>('./data/settings.json');
    }

}
export default settingsService;