import express from 'express';
// @ts-ignore
import cors from 'cors';
import {fileURLToPath} from "url";
import path from "path";
import toDoController from './toDo.controller.js';
import settingsController from './settings.controller.js';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const app = express()

app.use(cors());
app.use(express.static(__dirname + "/data"));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.raw());

// Add controllers
app.use(toDoController);
app.use(settingsController)

const port = 3000

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`);
})



