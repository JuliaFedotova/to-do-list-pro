import { Router } from 'express';
import todoService from './toDo.service.js';
import {getErrorMessage} from "./utils/getErrorMessage.js";
// @ts-ignore
import multipart from 'connect-multiparty';

const router = Router();
const multipartMiddleware = multipart();

router.post('/notes', multipartMiddleware, (req, res) => {
    try {
        res.send(todoService.addNote(req.body.title));
    } catch (e) {
        res.status(500);
        res.send(getErrorMessage(e));
    }
})

router.get('/notes', (req, res) => {
  try {
      res.send(todoService.getNote());
  } catch (e) {
      res.status(500);
      res.send(getErrorMessage(e));
  }
})

router.get('/deleted-notes', (req, res) => {
    try {
        res.send(todoService.getDeletedNotes());
    } catch (e) {
        res.status(500);
        res.send(getErrorMessage(e));
    }
})

router.get('/checked-notes', (req, res) => {
    try {
        res.send(todoService.getCheckedNotes());
    } catch (e) {
        res.status(500);
        res.send(getErrorMessage(e));
    }
})

router.post('/notes/:id/restore', multipartMiddleware, (req, res) => {
    try {
        res.send(todoService.restoreNote(Number(req.params.id)));
    } catch (e) {
        res.status(404);
        res.send(getErrorMessage(e));
    }
})

router.post('/notes/:id/uncheck', (req, res) => {
    try {
        res.send(todoService.uncheckNote(Number(req.params.id)));
    } catch (e) {
        res.status(404);
        res.send(getErrorMessage(e));
    }
})

router.post('/notes/:id/check', (req, res) => {
    try {
        res.send(todoService.checkNote(Number(req.params.id)));
    } catch (e) {
        res.status(404);
        res.send(getErrorMessage(e));
    }
})

router.delete('/notes/:id', (req, res) => {
    try {
        res.send(todoService.deleteNote(Number(req.params.id)));
    } catch (e) {
        res.status(404);
        res.send(getErrorMessage(e));
    }
})

router.put('/notes/:id', multipartMiddleware, (req, res) => {

    try {
        res.send(todoService.updateNote(Number(req.params.id), req.body.title))
    } catch(e) {
        res.status(500);
        res.send(getErrorMessage(e));
    }

})

export default router;
