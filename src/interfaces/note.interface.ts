export interface Note {
    title: string;
    id: number;
    deleted_at: string;
    NoteStatus: string;
}