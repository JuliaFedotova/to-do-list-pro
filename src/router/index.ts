import {createRouter, createWebHistory} from "vue-router"
import ToDoList from "../views/ToDoList.vue"
import DeletedNotesView from "../views/DeletedNotesView.vue"
import CheckedNotesView from "../views/CheckedNotesView.vue"

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: "/",
            name: "home",
            component: ToDoList
        },
        {
            path: "/checked",
            name: "checked",
            component: CheckedNotesView
        },
        {
            path: "/deleted",
            name: "deleted",
            component: DeletedNotesView
        }
    ]
})

export default router
