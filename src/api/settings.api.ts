export const settingsApi = {

    async getSettings() {
        const response = await fetch('http://localhost:3000/settings');
        return await response.json();
    },

    async addImage(formData: FormData) {

        const response = await fetch('http://localhost:3000/settings', {
            method: 'post',
            body: formData,
        })

        return await response.json();
    },
}