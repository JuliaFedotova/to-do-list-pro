import type {Notes} from "@/hooks/useNotes";
import type {Note} from "../../backend/interfaces/note.interface";

export const notesApi = {
    async getNotes() {
        const response = await fetch('http://localhost:3000/notes');
        return await response.json();

    },

    async getDeletedNotes() {
        const response = await fetch('http://localhost:3000/deleted-notes');

        return await response.json();
    },

    async getCheckedNotes() {
        const response = await fetch('http://localhost:3000/checked-notes');

        return await response.json();
    },

    async addNotes(formData: FormData) {

        const response = await fetch('http://localhost:3000/notes', {
            method: 'post',
            body: formData,
        })

        return await response.json();
    },

    async deleteNotes(notesId: number): Promise<Notes[]> {
        const response = await fetch(`http://localhost:3000/notes/${notesId}`, {
            method: 'delete',
        })

        const json = await response.json();

        if(!response.ok) {
            throw new Error(json.error);
        }

        return json;
    },

    async checkNotes(notesId: number): Promise<Notes[]> {
        const response = await fetch(`http://localhost:3000/notes/${notesId}/check`, {
            method: 'post',
        })

        const json = await response.json();

        if(!response.ok) {
            throw new Error(json.error);
        }

        return json;
    },

    async restoreNotes(notesId: number): Promise<Notes[]> {
        const response = await fetch(`http://localhost:3000/notes/${notesId}/restore`, {
            method: 'post',
        });

        return await response.json();
    },

    async unCheck(notesId: number): Promise<Notes[]> {
        const response = await fetch(`http://localhost:3000/notes/${notesId}/uncheck`, {
            method: 'post',
        });

        return await response.json();
    },

    async updateNote(notesId: number, title: string): Promise<Note> {
        const response = await fetch(`http://localhost:3000/notes/${notesId}`, {
            method: 'put',
            body: JSON.stringify({
                title,
            }),
            headers: {
                "Content-Type": "application/json"
            }
        });

        return await response.json();
    }
}