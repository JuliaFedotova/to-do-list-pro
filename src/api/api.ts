import {notesApi} from "@/api/notes.api";
import {settingsApi} from "@/api/settings.api";

export const api = {
    notes: notesApi,
    settings: settingsApi,
}
