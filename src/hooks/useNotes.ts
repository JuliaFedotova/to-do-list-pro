import {ref} from 'vue';
import {api} from "@/api/api";

export interface Notes  {
    title: string;
    id: number;
    deleted_at: string;
    NoteStatus: string;
}

const notesList = ref<Notes[]>([]);
const deletedNoteList = ref<Notes[]>([]);
const checkedList = ref<Notes[]>([]);

export function useNotes() {

    async function loadNotes() {
        notesList.value = await api.notes.getNotes();
    }

    async function loadDeletedNotes() {
        deletedNoteList.value = await api.notes.getDeletedNotes();
    }

    async function loadCheckedNotes() {
        checkedList.value = await api.notes.getCheckedNotes();
    }


    return {
        notesList,
        deletedNoteList,
        checkedList,
        loadNotes,
        loadDeletedNotes,
        loadCheckedNotes,
    }
}