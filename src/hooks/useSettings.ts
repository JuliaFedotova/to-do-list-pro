import {ref} from 'vue';
import type {Settings} from "@/interfaces/settings.interface";
import {api} from "@/api/api";

const isOpen = ref(false);

const settings = ref<Settings>({
    backgroundPath: 'src/assets/images/обои_1.jpeg',
});

export const BackgroundImages = [
    { path: 'src/assets/images/обои_1.jpeg'},
    { path: 'src/assets/images/обои_2.jpeg'},
    { path: 'src/assets/images/обои_3.jpeg'},
    { path: 'src/assets/images/обои_4.jpeg'},
    { path: 'src/assets/images/обои_5.jpeg'},
    { path: 'src/assets/images/обои_6.jpeg'},
    { path: 'src/assets/images/обои_7.jpeg'},
    { path: 'src/assets/images/обои_8.jpeg'},

];

export function useSettings() {

    async function openPopup() {
        isOpen.value = true;
    }

    async function loadSettings() {
        settings.value = await api.settings.getSettings();
    }

    async function selectBackground(backgroundPath: string) {
        const formData = new FormData();

        formData.append("backgroundPath", backgroundPath);

        settings.value = await api.settings.addImage(formData);
    }

    return {
        isOpen,
        BackgroundImages,
        openPopup,
        settings,
        loadSettings,
        selectBackground,
    }
}